# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: shuertas <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/21 14:10:28 by shuertas          #+#    #+#              #
#    Updated: 2018/05/01 18:39:12 by shuertas         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

C_GREEN 	= \033[32;01m
C_NO 		= \033[0m
C_RED 		= \033[31;01m
C_YELLOW 	= \033[33;01m
C_CYAN		= \033[36;01m
C_PINK		= \033[35;01m
C_BLACK 	= \033[30;01m

################################################################################
##[ SOURCES ]###################################################################
################################################################################

SRCS		= 	./src/tiny-verse.c \
				./src/organism/genome_new.c \
				./src/organism/genome_print.c \
				./src/organism/organism_new.c \
				./src/organism/organism_zero.c \
				./src/organism/limbs_generate.c

OBJS		= $(SRCS:.c=.o)

NAME		= tiny-verse
FLAGS		= -Wall -Wextra -Werror -g
CC			= gcc
INCLUDES	= -I ./inc/ -I ./libct/
LIB			= libct/libct.a

BIN_NUM		= 0
BIN_MAX		= $(shell echo $(OBJS) | wc -w)

################################################################################
##[ RULES ]#####################################################################
################################################################################

##############################
##[ MAIN RULE ]###############
##############################
all: $(NAME)

##############################
##[ LIBRARY RULE ]############
##############################
$(NAME): $(OBJS)
	@tput cuu1 ; tput cuf 1
	@printf "100"
	@tput cnorm
	@rm -rf .bin_num .bin_num_tmp .bin_state
	@printf "\n$(C_GREEN)SOURCES $(C_BLACK)[$(C_YELLOW)OK$(C_BLACK)]$(C_NO)\n"
	@make -C libct
	@gcc $(FLAGS) $(OBJS) $(LIB) -o $(NAME)
	@printf "$(C_GREEN)TINY-VERSE $(C_BLACK)[$(C_YELLOW)DONE$(C_BLACK)]"
	@printf "$(C_NO)\n"

##############################
##[ COMPILATION RULE ]########
##############################
%.o: %.c Makefile inc/tiny-verse.h inc/organism.h libct/libct.h
	@if [ -z ${INTRO} ]; then \
		tput civis; \
		printf "$(C_CYAN)[$(C_YELLOW)000 $(C_PINK)%%$(C_CYAN)]$(C_YELLOW)\n"; \
	fi
	$(eval INTRO := 1)
	@gcc $(FLAGS) $(INCLUDES) -o $@ -c $< || (tput cnorm && FAIL 2>&-)
	@$(eval BIN_NUM		:= $(shell echo "${BIN_NUM}+1" | bc))
	@$(eval BIN_STATE 	:= $(shell echo "${BIN_NUM}*100/${BIN_MAX}" | bc))
	@tput cuu1 ; tput cuf 1
	@printf "%.3d\n" ${BIN_STATE}

##############################
##[ CLEANING RULES ]##########
##############################
clean:
	@rm -rf $(OBJS)

fclean: clean
	@rm -rf $(NAME)

##############################
##[ RESTART RULES ]###########
##############################
re: fclean
	@make
