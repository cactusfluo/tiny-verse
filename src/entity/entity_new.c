
#include "tiny-verse.h"

////////////////////////////////////////////////////////////////////////////////
/// STATIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static t_entity_limb	**load_limbs(t_genome *genome)
{
	t_entity_limb		**limbs;
	uint16_t			i;

	genome = NULL; // TMP
	limbs = calloc(6, sizeof(t_entity_limb*));
	if (limbs == NULL)
		return (NULL);
	for (i = 0; i < 5; i++)
	{
		limbs[i] = malloc(sizeof(t_entity_limb));
		if (limbs[i] == NULL)
			return (NULL);
	}
	limbs[5] = NULL;
	return (limbs);
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

t_entity				*entity_new(t_genome *genome)
{
	t_entity			*new_entity;

	if (genome == NULL)
		return (NULL);
	new_entity = malloc(sizeof(t_entity));
	if (new_entity == NULL)
		return (NULL);
	new_entity->genome = genome;
	new_entity->limbs = load_limbs(genome);
	return (new_entity);
}
