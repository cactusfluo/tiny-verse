
#include "tiny-verse.h"

////////////////////////////////////////////////////////////////////////////////
/// STATIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////
// Returns the limb sequence
// length.
//////////////////////////////
static inline uint16_t	end_of_limb(char *genes, uint16_t length)
{
	uint16_t			limb_length;

	limb_length = 0;
	while (1)
	{
		if (limb_length == length)
			return (limb_length);
		if (genes[limb_length] == '0'
		&& (limb_length == length - 1 || genes[limb_length + 1] == '0'))
			return (limb_length + 1);
		limb_length++;
	}
}

//////////////////////////////
// Creates the limb from its
// genes sequence.
//////////////////////////////
static t_organism_limb	*new_limb(char *genes, uint16_t length)
{
	// TO DO
	return (malloc(sizeof(t_organism_limb)));
}

//////////////////////////////
// Recursively loads every
// organism limbs.
//////////////////////////////
static t_organism_limb	**load_limb(char *genes, uint16_t length, uint16_t i)
{
	t_organism_limb		**array;
	t_organism_limb		*new_limb;
	uint16_t			limb_length;

	limb_length = end_of_limb(genes, length);
	if (limb_length == 0)
		return (calloc(i + 1, sizeof(t_organism_limb*))); // empty limbs array
	array = load_limb(genes + limb_length, length - limb_length, i + 1);
	if (array == NULL)
	{
		// error handling
		return (NULL);
	}
	array[i] = new_limb(genes, limb_length);
	return (array);
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

/*
**
** To get the main functionnement of this function, see "ct_strsplit.c".
**  The splitting system is the same, avoiding double iteration.
**
*/
t_organism_limb			**limbs_generate(t_genome *genome)
{
	t_organism_limb		**limbs;

	limbs = load_limb(genome->genes + GENOME_HEADER_LENGTH,
		- GENOME_HEADER_LENGTH, genome->length, 0);
	return (limbs);
}
