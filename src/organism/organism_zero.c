
#include "tiny-verse.h"

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

t_organism		*organism_zero(void)
{
	t_organism	*new_organism;
	t_genome	*genome;

	genome = genome_new(0);
	if (genome == NULL)
		return (NULL);
	genome->genes[0] = '1';
	new_organism = organism_new(genome);
	if (new_organism == NULL)
	{
		free(genome);
		return (NULL);
	}
	return (new_organism);
}
